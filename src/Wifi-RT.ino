// IMPORTATION de librairies
#include <LWiFi.h>
#include <LWiFiClient.h>
#define WIFI_AP "RT-WIFI-Guest"            
#define WIFI_PASSWORD "wifirtguest" 
#define WIFI_AUTH LWIFI_WPA           // Authentification par WPA             
char bssid[18];             // Adresse MAC de la borne WIFI
void setup(){
  Serial.begin(9600);     
  while (!Serial) {
    ;                    
  }
  // Connetion au WiFi
  LWiFi.begin();        // Initialise le module WiFi
  Serial.print("Connection au WiFi : ");
  Serial.println(WIFI_AP);
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }
    // Statut de connection
  StatutWifi();
//  Serial.println(LWiFi.BSSID()); // Affiche l'adresse MAC du routeur
  listNetworks(); //Scan réseau
    Serial.println();
    
}
void loop() {
 
}
// Affichage du statut WiFi
void StatutWifi() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  // print BSSID
  uint8_t BSSID[VM_WLAN_WNDRV_MAC_ADDRESS_LEN] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("BSSID is : ");  
  sprintf(bssid, "%2X:%2X:%2X:%2X:%2X:%2X", BSSID[0], BSSID[1], BSSID[2], BSSID[3], BSSID[4], BSSID[5]);
  Serial.println(bssid);
}
  void listNetworks() {
  // scan for nearby networks:
  Serial.println("** Scan Networks **");
  // Obtenir le nombre de points d'accès
int numOfAP = LWiFi.scanNetworks();
  // print the list of networks seen:
    Serial.print("number of available networks:");
    Serial.println(numOfAP);
  // print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numOfAP; thisNet++) {
    Serial.print(thisNet);
    Serial.print(") ");
    Serial.print(LWiFi.SSID(thisNet));
    Serial.print("\tSignal: ");
    Serial.print(LWiFi.RSSI(thisNet));
    Serial.print(" dBm\t");
  }
}
