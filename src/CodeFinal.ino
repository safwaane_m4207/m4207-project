//Batterie
#include <LBattery.h> //Ajout de la bibliothèque pour la batterie
#include <Wire.h> //Ajout de la bibliothèque i2c pour utiliser le lcd
#include "rgb_lcd.h" //Ajout de la bibliothèque du lcd

#define LED 2  // On indique où est branchée la LED
#define BUZ 6 // On indique ou est branchée le Buzzer
#define LCD 12 //On indique où est branchée le LCD
char buff[256];

//Wifi
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#define WIFI_RT_GUEST_AP "RT-WIFI-Guest"
#define WIFI_RT_GUEST_PASSWORD "wifirtguest"
#define WIFI_RT_GUEST_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WIFI_EDU_AP "eduspot"
#define WIFI_EDU_PASSWORD ""
#define WIFI_EDU_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WL_MAC_ADDR_LENGTH 6
#define ROW_COUNT(array)    (sizeof(array) / sizeof(*array))

//Adresses MAC
char* rt_guest_salle[6][2] = {
  {"La clef est dans l'administration RT","80:2A:A8:5A:5C:68:"},
  {"La clef est en salle PROJ-DOC","80:2A:A8:4A:AD:26:"},
  {"La clef est en salle Reseaux et cablages","80:2A:A8:4A:AD:2E:"},
  {"la clef est en salle Signaux et systemes","80:2A:A8:4A:AF:2E:"},
  {"La clef est en salle TD1","80:2A:A8:47:CE:A5:"},
  {"La clef est en salle TD3","80:2A:A8:4A:AC:55:"}
};
char* eduspot_salle[8][2] = {
  {"La clef est dans l'administration RT","0:1D:A2:D8:38:B1:"},
  {"La clef est en salle TD2","0:1D:A2:D8:3D:E1:"},
  {"La clef est en salle Signaux et systemes","0:14:A8:39:2C:B6:"},
  {"La clef est en salle PROJ-DOC","0:14:6A:F0:45:51:"},
  {"La clef est en salle Reseaux et cablages","0:14:6A:F0:42:21:"},
  {"La clef est en salle Micro informatique pogrammation","0:14:6A:F0:41:31:"},
  {"La clef est en salle Genie informatique","0:1D:A2:D8:3D:E1:"}
};
int i =0;
char addr_mac[17];
LWiFiServer server(80);

  rgb_lcd lcd; //Initialisation LCD
  int niveau; //Variables pour la batterie
  int charge; //Variables pour la batterie

void setup() {
  lcd.begin(16, 2); //Initialisation du lcd de 16 colonnes et 2 lignes
  LWiFi.begin(); //Démarrage Wifi
  Serial.begin(115200);
  pinMode(LED,OUTPUT); //LED
  pinMode(BUZ,OUTPUT); //BUZZER
}

void loop() {
   niveau = LBattery.level();
  charge = LBattery.isCharging();
  delay(5000);

  
  //Condition suivant le niveau de batterie
  while (charge == 1) {
    lcd.setCursor(0,0); //On commence à écrire en haut à gauche
    lcd.print("Charging : "); //On écrit un texte
    lcd.print(niveau); //On écrit un texte
    lcd.print("%");
    digitalWrite(LED, HIGH);   // turn on the LED 
    delay(500);                       
    digitalWrite(LED, LOW);    // turn off the LED
    delay(500);
}
  //Condition si on utilise la batterie
   while (charge==0){
    lcd.setCursor(0,0); //On commence à écrire en haut à gauche
    lcd.print("Batterie : "); //On écrit un texte
    lcd.print(niveau); //On écrit un texte
    lcd.print("%");
    digitalWrite(LED, HIGH);
    delay(2000);
    digitalWrite(LED, LOW);
    delay(2000);

  if ((niveau <= 66) && (niveau > 33)){
    lcd.setRGB(204,255,153);
  }
  
  if (niveau <= 33){
    lcd.noDisplay();
    delay(500);
    lcd.display();
    lcd.setRGB(204,0,0);
    delay(500);
    lcd.noDisplay();
    delay(500);
    lcd.display();
    lcd.setRGB(255,255,0);
    delay(500);
    
    digitalWrite(LED, HIGH);   // turn on the LED 
    digitalWrite(BUZ, HIGH); // buzz out !!!
    delay(500);                       
    digitalWrite(LED, LOW);    // turn off the LED
    digitalWrite(BUZ, LOW); // buzz down !!!
    delay(500);
  }
    
  }
  
  // Affiche l'état de la batterie dans le moniteur série
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  delay(1000); 

  //Recherche des routeurs wifi
  getInfo(WIFI_EDU_AP, WIFI_EDU_AUTH, WIFI_EDU_PASSWORD);
  getInfo(WIFI_RT_GUEST_AP, WIFI_RT_GUEST_AUTH, WIFI_RT_GUEST_PASSWORD);
  Serial.print('\n');
  Serial.print('\n');

//Sonnerie et clignotement en cas de batterie faible
if (LBattery.level() <= 100) {
/* Le linkit one ne connait que 4 niveaux de batterie
  100, 66, 33, 0 %
*/
 digitalWrite(LED, HIGH);   // turn on the LED 
 digitalWrite(BUZ, HIGH); // buzz out !!!
 delay(500);                       
 digitalWrite(LED, LOW);    // turn off the LED
 digitalWrite(BUZ, LOW); // buzz down !!!
 delay(500);    

}
}

//Affichage des infos Wifi
void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  // print BSSID (MAC Adr)
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("AP's MAC Address is: ");
  sprintf(addr_mac,"%2X:%2X:%2X:%2X:%2X:%2X:",BSSID[0],BSSID[1],BSSID[2],BSSID[3],BSSID[4],BSSID[5]);
  Serial.print(addr_mac);
  Serial.println();
}
void getInfo(char *rssi, LWiFiEncryption auth, char *psswd)
{
  LWiFi.disconnect();
  while (0 == LWiFi.connect(rssi, LWiFiLoginInfo(auth, psswd)))
  {
    delay(1000);
  }
  printWifiStatus();
  salle(addr_mac);
}
void salle(char *mac)
{
  for(i=0;i<ROW_COUNT(rt_guest_salle);i++){
    if(strcmp(mac,rt_guest_salle[i][1])== 0){
       Serial.print(rt_guest_salle[i][0]);
    }
  }
  for(i=0;i<ROW_COUNT(eduspot_salle);i++){
    if(strcmp(mac,eduspot_salle[i][1])== 0){
       Serial.print(eduspot_salle[i][0]);
    }
  }
  
}

