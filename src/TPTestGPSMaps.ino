#include <LGPS.h>

gpsSentenceInfoStruct info;

char buff[256];

static unsigned char getComma(unsigned char num,const char *str)
{
unsigned char i,j=0;
int len=strlen(str);

for(i = 0;i < len;i ++)
 {
  if(str[i]==',')
  j++;


if(j==num)
  return i+1;

}
return 0;

}

static double getDoubleNumber(const char *s){
  char buf[10];

unsigned char i;

double rev;
i=getComma(1,s);
i = i - 1;
strncpy(buf,s,i);
buf[i]=0;
rev=atof(buf);
return rev;
}

static double getIntNumber(const char *s){char buf[10];

unsigned char i;
double rev;
i=getComma(1,s);
i = i - 1;
strncpy(buf,s,i);
buf[i]=0;
rev=atoi(buf);
return rev;
}

void parseGPGGA(const char *GPGGAstr){
double latitude;
double longitude;
int tmp,hour,minute,second,num;
tmp=getComma(2,GPGGAstr);
latitude=getDoubleNumber(&GPGGAstr[tmp]);
tmp=getComma(4,GPGGAstr);
longitude=getDoubleNumber(&GPGGAstr[tmp]);

sprintf(buff,"https://www.google.com/maps/@%10.4f,%10.4f,17z",latitude,longitude);

//%10.4fSerial.println(buff);}

void setup() {// put your setup code here, to run once:

Serial.begin(115200);

LGPS.powerOn();

Serial.println("LGPS Power on, and waiting ...");

delay(3000);
}

void loop() {

LGPS.getData(&info);

parseGPGGA((constchar*)info.GPGGA);

delay(2000);
}
