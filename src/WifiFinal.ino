/*
 WiFi Web Server
 A simple web server that a repeated counter
 Change the macro WIFI_AP, WIFI_PASSWORD and WIFI_AUTH accordingly.
 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 modified 20 Aug 2014
 by MediaTek Inc.
 */
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#define WIFI_RT_GUEST_AP "RT-WIFI-Guest"
#define WIFI_RT_GUEST_PASSWORD "wifirtguest"
#define WIFI_RT_GUEST_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WIFI_EDU_AP "eduspot"
#define WIFI_EDU_PASSWORD ""
#define WIFI_EDU_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration
#define WL_MAC_ADDR_LENGTH 6
#define ROW_COUNT(array)    (sizeof(array) / sizeof(*array))
char* rt_guest_salle[6][2] = {
  {"La clef est dans l'administration RT","80:2A:A8:5A:5C:68:"},
  {"La clef est en salle PROJ-DOC","80:2A:A8:4A:AD:26:"},
  {"La clef est en salle Reseaux et cablages","80:2A:A8:4A:AD:2E:"},
  {"la clef est en salle Signaux et systemes","80:2A:A8:4A:AF:2E:"},
  {"La clef est en salle TD1","80:2A:A8:47:CE:A5:"},
  {"La clef est en salle TD3","80:2A:A8:4A:AC:55:"}
};
char* eduspot_salle[8][2] = {
  {"La clef est dans l'administration RT","0:1D:A2:D8:38:B1:"},
  {"La clef est en salle TD2","0:1D:A2:D8:3D:E1:"},
  {"La clef est en salle Signaux et systemes","0:14:A8:39:2C:B6:"},
  {"La clef est en salle PROJ-DOC","0:14:6A:F0:45:51:"},
  {"La clef est en salle Reseaux et cablages","0:14:6A:F0:42:21:"},
  {"La clef est en salle Micro informatique pogrammation","0:14:6A:F0:41:31:"},
  {"La clef est en salle TD1","0:1D:A2:D8:3D:E1:"},
  {"La clef est en salle Genie informatique","0:1D:A2:D8:3D:E1:"}
};
int i =0;
char addr_mac[17];
LWiFiServer server(80);
void setup()
{
  LWiFi.begin();
  Serial.begin(115200);
}
void loop()
{
  getInfo(WIFI_EDU_AP, WIFI_EDU_AUTH, WIFI_EDU_PASSWORD);
  getInfo(WIFI_RT_GUEST_AP, WIFI_RT_GUEST_AUTH, WIFI_RT_GUEST_PASSWORD);
}
void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  // print BSSID (MAC Adr)
  uint8_t BSSID[WL_MAC_ADDR_LENGTH] = {0};
  LWiFi.BSSID(BSSID);
  Serial.print("AP's MAC Address is: ");
  sprintf(addr_mac,"%2X:%2X:%2X:%2X:%2X:%2X:",BSSID[0],BSSID[1],BSSID[2],BSSID[3],BSSID[4],BSSID[5]);
  Serial.print(addr_mac);
  Serial.println();
}
void getInfo(char *rssi, LWiFiEncryption auth, char *psswd)
{
  LWiFi.disconnect();
  while (0 == LWiFi.connect(rssi, LWiFiLoginInfo(auth, psswd)))
  {
    delay(1000);
  }
  printWifiStatus();
  salle(addr_mac);
}
void salle(char *mac)
{
  for(i=0;i<ROW_COUNT(rt_guest_salle);i++){
    if(strcmp(mac,rt_guest_salle[i][1])== 0){
       Serial.print(rt_guest_salle[i][0]);
    }
  }
  for(i=0;i<ROW_COUNT(eduspot_salle);i++){
    if(strcmp(mac,eduspot_salle[i][1])== 0){
       Serial.print(eduspot_salle[i][0]);
    }
  }
  
}
