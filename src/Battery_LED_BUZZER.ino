#include <LBattery.h>
#define LED 2  // On indique où est branchée la LED
#define BUZ 6 // On indique ou est branchée le Buzzer
char buff[256];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  pinMode(BUZ,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  delay(1000); 

if (LBattery.level() <= 100) {
/* Le linkit one ne connait que 4 niveaux de batterie
  100, 66, 33, 0 %
*/
 digitalWrite(LED, HIGH);   // turn on the LED 
 digitalWrite(BUZ, HIGH); // buzz out !!!
 delay(500);                       
 digitalWrite(LED, LOW);    // turn off the LED
 digitalWrite(BUZ, LOW); // buzz down !!!
 delay(500);    

}
}
