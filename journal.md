# Bilan séance 1 :
-J'ai un code pour localiser le Linkit One et ouvrir la localisation avec un URL dans Google Maps. Cependant, pour l'instant on ne trouve pas de satellites.
-J'arrive à faire clignoter la LED lorsque la batterie est faible.

-Pour la prochaine fois, il faudrait aussi faire sonner le buzzer lorsque la batterie est faible.

# Bilan séance 2 :
-Etant donné que le GPS ne fonctionnait pas, on pourrait envisager une solution avec le wifi : Il faudrait se connecter au réseau wifi le plus proche, identifier son adresse MAC et l'identifier par rapport à une base de données (à établir) comprenant les adresses MAC des routeurs situés dans les différentes salles. Pour cela, j'ai commencé à utiliser un code, mais il faudra bien le tester lorsque le RT-WIFI sera rétabli. 
-Lorsque la batterie est faible, j'arrive aussi à faire sonner le buzzer. 

-Pour la prochaine fois, je pourrais chercher une solution pour afficher le % de batterie restant sur l'écran LCD, mais le plus important restera la localisation du Linkit One en fonction du réseau wifi auquel il est connecté. 

# Bilan séance 3 :
-Pour identifier celui qui prend la clé, on pourrait soit enregistrer un message vocal, soit utiliser un système d'envoi d'identifiant (numéro étudiant) par Bluetooth. Cependant, je n'ai pas eu le temps de le faire. La solution par bluetooth semble la meilleure, ou alors un envoi un de SMS, en utilisant une carte SIM.



#Bilan séance 4 : 
-La localisation en utilisant RT-WIFI n'est pas assez précise en raison du nombre de routeurs (étant donné qu'un routeur couvre 2 à 3 salles). Par conséquent, j'ai modifié le code pour l'adapter au réseau Eduspot. On remarque que ça fonctionne assez bien. 
-Pour la prochaine fois, il faudrait d'afficher directement la salle dans laquelle on se trouve en fonction de l'adresse MAC du routeur auquel on est connecté. 

# Bilan séance 5 : 
-J'arrive à scanner les réseaux Eduspot + RT et afficher la salle dans laquelle on est, en faisant référence aux adresses MAC. 
-Le prochain travail est d'afficher les infos de localisation de toutes les clés sur un serveur Web.