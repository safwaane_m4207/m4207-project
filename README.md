## Projet de localisation des clés du département à l'aide du Linkit One
Groupe "1" : PATEL Safwaane

### Matériel nécessaire
Pour tester mon code, on a besoin de l'antenne GPS, de l'antenne WIFI, du Base Shield, de la batterie, de l'écran LCD, d'une LED et du Buzzer

### Position des équipements
Dans ma configuration, il faut monter le "Base Shield" sur le Linkit One, et brancher la LED sur le port 2, le Buzzer sur le port 6 et l'écran LCD sur le port 12C.